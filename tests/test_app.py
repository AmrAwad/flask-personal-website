import unittest
from app import create_app
import io
import os
import shutil

class TestApp(unittest.TestCase):
    def setUp(self):
        self.app = create_app()
        self.ctx = self.app.app_context()
        self.ctx.push()
        self.client = self.app.test_client(use_cookies=True)


    def tearDown(self):
        self.ctx.pop()
        if os.path.exists('uploads'):
            shutil.rmtree('uploads')

    def uploadFile(self, file_name, file_content):
        return self.client.post('/upload', buffered=True,
                    content_type='multipart/form-data',
                    follow_redirects=False,
                    data={'file' : (io.BytesIO(file_content.encode('utf-8')), file_name)})

    def test_home(self):
        r = self.client.get('/')

        self.assertEqual(r.status_code, 200)
        self.assertTrue("<h1>Hello There</h1>" in r.get_data(as_text=True))

    def test_UploadRequestWithFile_ReturnNameAndContent(self):
        file_name = "hello.txt"
        file_content = "hello there"

        r = self.uploadFile(file_name, file_content)

        self.assertEqual(r.status_code, 200)
        self.assertEqual(file_name, r.get_json().get('file_name'))
        self.assertEqual(file_content, r.get_json().get('file_content'))
        self.assertTrue(os.path.exists(os.path.join('uploads', file_name))) 

    def test_UploadTwoFiles_BothShouldExist(self):
        file_name_1 = "hello1.txt"
        file_name_2 = "hello2.txt"
        file_content = "hello there"

        r = self.uploadFile(file_name_1, file_content)

        self.assertEqual(r.status_code, 200)
        self.assertTrue(os.path.exists(os.path.join('uploads', file_name_1))) 

        r = self.uploadFile(file_name_2, file_content)

        self.assertEqual(r.status_code, 200)
        self.assertTrue(os.path.exists(os.path.join('uploads', file_name_2))) 


    def test_UploadRequestWithoutFile_ShouldFail(self):
        r = self.client.post('/upload', buffered=True)

        self.assertEqual(r.status_code, 404)

    def test_DisplayRequest_ReturnsJson(self):
        r = self.client.get('/display')

        self.assertEqual(r.status_code, 200)
        self.assertEqual("ok", r.get_json().get('status'))
        self.assertEqual("here is the content", r.get_json().get('content'))

if __name__ == '__main__':
    unittest.main()