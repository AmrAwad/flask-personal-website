# Flask Web App 

## Usage

To run the application use:
```
python run.py
```

To run the tests use:
```
python test.py
```

## Use Cases

### Use Case 1:

1. User goes to home page
2. User uploads file with correct extension
3. User gets a loading icon while processing on the same page
4. After processing is done, user gets a button.
5. User clicks button to go to results page.

### Use Case 2:
1. User goes to home page
2. User uploads file with wrong extension
3. User gets a warning and can restart @ homepage again