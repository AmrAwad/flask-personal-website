from flask import Blueprint, render_template, request
from .models import db, Project
from .form import RegistrationForm


main = Blueprint('main', __name__)

POSTS_PER_PAGE = 6

@main.route('/')
def home():
    page = request.args.get('page', 1, type=int)
    projects = Project.query.paginate(page, POSTS_PER_PAGE, False)
    return render_template("projects.html", projects=projects)

@main.route('/add', methods=['GET', 'POST'])
def add():
    form = RegistrationForm(request.form)
    if request.method == 'POST' and form.validate_on_submit():
        project = Project()
        form.populate_obj(project)
        db.session.add(project)
        db.session.commit()
    return render_template('add_project.html', form=form)
