from flask import Flask
from flask_bootstrap import Bootstrap
from .main import main as main_blueprint
from .models import db


def create_app():
    app = Flask(__name__)
    app.config['BOOTSTRAP_SERVE_LOCAL'] = True

    Bootstrap(app)

    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///database.db'
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    app.config['SECRET_KEY'] = 'meow'


    db.init_app(app)

    app.register_blueprint(main_blueprint)

    return app